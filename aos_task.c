/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_task.c
 *
 * @brief contains implementation of task specific routines
 */


#include "aos_task.h"
#include "aos_sched.h"

#include <string.h>
#include <avr/interrupt.h>


/* ================================================================================ */

/**
 * @brief wrapper created to enable task launching with the argument
 */
static void _aos_task_launcher(void);


/* ================================================================================ */


struct task_cb* aos_task_create(task_proc_t a_task_proc, void *a_pdata, uint8_t a_prio, size_t a_stack) {

	uint16_t blk_size = AOS_CALC_TASK_WA_SIZE(a_stack);

	AOS_LOCK();

	struct task_cb *cb = (struct task_cb *)malloc(blk_size);

	if (!cb) {
		_aos_common_hook_exec(AOS_HOOK_OOM);
		return NULL;
	}

	memset(cb, 0x00, blk_size);
	cb->wa = (void *)((uint8_t *)cb + sizeof(struct task_cb));
	cb->wa_size = (blk_size - sizeof(struct task_cb));
	
	cb->proc = a_task_proc;
	cb->pdata = a_pdata;
	cb->systicks = 0x00;
	
	// initialize stack pointer
	cb->ctx.sp = (struct aos_machine_ctx *)((uint8_t *)cb + 
			blk_size - 
			AOS_MACHINE_CTX_SIZE() -
			AOS_IRQ_CTX_SIZE());

	// setup initial context
	// callback
	cb->ctx.sp->r[2]  = ((uint16_t)a_task_proc) & 0xff;
  	cb->ctx.sp->r[3]  = ((uint16_t)a_task_proc >> 8) & 0xff;

	// arguments
  	cb->ctx.sp->r[4]  = ((uint16_t)a_pdata) & 0xff;
  	cb->ctx.sp->r[5]  = ((uint16_t)a_pdata >> 8) & 0xff;

	// read status register
	cb->ctx.sp->sreg = SREG;

	// task launcher will be first called
	cb->ctx.sp->pc.pc8.lo = ((uint16_t)_aos_task_launcher >> 8) & 0xff;
	cb->ctx.sp->pc.pc8.hi = ((uint16_t)_aos_task_launcher) & 0xff;

	// suspended initially
	aos_task_priority_set(cb, a_prio);

	// if it's not an IDLE task, then add it to run-list
	if (AOS_TASK_PRIORITY_IDLE != a_prio) {
		// mark as suspended
		aos_task_state_set(cb, AOS_TASK_SUSPENDED);

		// add to run list
		aos_sched_task_resume(cb);
	}

	AOS_UNLOCK();
	return cb;
}


__inline__ 
void aos_task_priority_set(struct task_cb *a_task, uint8_t a_prio) {
	a_task->prio_state &= 0x0f;
	a_task->prio_state |= ((a_prio << 4) & 0xf0);
}


__inline__ 
void aos_task_state_set(struct task_cb *a_task, uint8_t a_state) {
	a_task->prio_state &= 0xf0;
	a_task->prio_state |= (a_state & 0x0f);
}


__inline__ 
uint8_t aos_task_priority_get(struct task_cb *a_task) {
	return ((a_task->prio_state & 0xf0) >> 4);
}


__inline__ 
uint8_t aos_task_state_get(struct task_cb *a_task) {
	return (a_task->prio_state & 0x0f);
}


/* ================================================================================ */


void _aos_task_exit(void) {
  // call the task exit hook
  _aos_common_hook_exec(AOS_HOOK_TASK_EXIT);

  // TODO setting the task exit status and reclaiming the resources if possible

  // we will land up here if the task will exit suddenly
  while (1);
}


__attribute__((naked))
static void _aos_task_launcher(void) {
  __asm__ volatile ("movw    r24, r4");
  __asm__ volatile ("movw    r30, r2");
  __asm__ volatile ("icall");
  // task is running now

  __asm__ volatile ("call _aos_task_exit");
}


/* ================================================================================ */


