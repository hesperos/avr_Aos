#ifndef AOS_TASK_H_QTCJHUI1
#define AOS_TASK_H_QTCJHUI1


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_task.h
 *
 * @brief task control block interface
 *
 * Declares task control block and task related routines and interface
 */


#include "aos_sys_types.h"
#include "aos_ctx.h"
#include "aos_common.h"
#include "aos_constants.h"

// ================================================================================

/**
 * @brief this macro is used to determine the task "work area" size. 
 *
 * @param __size stack size requested by the task
 *
 * It returns how much memory is needed for a task, which wants to have 
 * a stack of size __size for it's private needs
 */
#define AOS_CALC_TASK_WA_SIZE(__size) \
	(__size + AOS_IRQ_CTX_SIZE() + AOS_MACHINE_CTX_SIZE() + sizeof(struct task_cb))

// ================================================================================

/**
 * @brief task procedure type declaration
 *
 * @param a_data private data for the task callback
 */
typedef void (*task_proc_t)(void *a_data);


/**
 * @brief task context block
 */
struct task_cb {
	
	/// task list pointers must be on top
	struct task_cb *prv, *nxt;

	/// holds current stack pointer, and reference to machine state
	struct aos_ctx ctx;

	/// task priority / task state combined field
	uint8_t prio_state;

	/// number of time quanta's task can consume
	uint8_t quanta;

	/// task execution handler
	task_proc_t proc;

	/// number of systicks consumed by the task
	aos_systime_t systicks;

	/// work area size
	uint16_t wa_size;

	/// work area pointer
	void *wa;

	/// task private data
	void *pdata;
};


// ================================================================================

/**
 * @brief Creates a system task and ads it to system runlist
 *
 * Creates the system task, returns the freshly created task control block and adds
 * the task to system runlist.
 *
 * @param a_task_proc function callback for the task
 * @param a_pdata any private data which will be passed as tasks argument, NULL if none.
 * @param a_prio task priority
 * @param a_stack stack for the task, a value of 32 bytes is recommended
 *
 * @return task control block
 */
struct task_cb* aos_task_create(task_proc_t a_task_proc, void *a_pdata, uint8_t a_prio, size_t a_stack);


/**
 * @brief configures task priority to a given value
 *
 * @param a_task task which priority will be changed
 * @param a_prio priority
 */
void aos_task_priority_set(struct task_cb *a_task, uint8_t a_prio);


/**
 * @brief set task state
 *
 * This function shouldn't be used outside of the system.
 *
 * @param a_task task
 * @param a_state state
 */
void aos_task_state_set(struct task_cb *a_task, uint8_t a_state);


/**
 * @brief return the priority of a specified task
 *
 * @param a_task task to be examined
 *
 * @return priority
 */
uint8_t aos_task_priority_get(struct task_cb *a_task);


/**
 * @brief returns task state
 *
 * @param a_task task
 *
 * @return state
 */
uint8_t aos_task_state_get(struct task_cb *a_task);


// ================================================================================


#endif /* end of include guard: AOS_TASK_H_QTCJHUI1 */
