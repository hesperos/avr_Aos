#ifndef AOS_SCHED_H_2RJAN3PE
#define AOS_SCHED_H_2RJAN3PE


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_sched.h
 *
 * @brief interface for the system scheduler
 *
 * @example sched_01.c
 *
 * Using scheduler routines.
 *
 * @example sched_02.c
 *
 * Using scheduler routines.
 */


#include "aos_task.h"
#include "aos_sys_types.h"


/**
 * @brief will sleep the task -> put it in the beginning of the wait queue, until task is resumed
 *
 * @param a_task task to be paused
 */
void aos_sched_task_sleep(struct task_cb *a_task);


/**
 * @brief will bring the task back from it's sleep state.
 *
 * The task will be rescheduled immediately if it has higher priority than the current one
 *
 * @param a_task task to be awoken
 */
void aos_sched_task_awake(struct task_cb *a_task);


/**
 * @brief will suspend the task -> put it at the end of the wait queue.
 *
 * The difference between sleeping a task and suspending it is very subtle. In fact, it is only
 * a logical distinction. The sleep state should be treated as a more temporary state, while suspend
 * state is meant to be more permanent. The only difference in implementation is that task being put to sleep
 * are placed at the beginning of the list - so they can be found quicker.
 *
 * @param a_task task to be suspended
 */
void aos_sched_task_suspend(struct task_cb *a_task);


/**
 * @brief resume a task, previously suspended
 *
 * @param a_task task to be resumed
 */
void aos_sched_task_resume(struct task_cb *a_task);


/**
 * @brief should be called by task to yield the scheduler, informing the system
 * that at the moment task has nothing to do and processing can be transfered to some
 * other task in need
 */
void aos_sched_yield();


/**
 * @brief called from tick interrupt as a request to reschedule a task
 *
 * This is a private API and should not be used outside the system code.
 */
void _aos_sched_tick_yield();


/**
 * @brief called when quanta of the current task expired. 
 *
 * This routine goes through run list and search for a new
 * task to be scheduled. If no task can be found, it will set the idle task
 * as the current one.
 *
 * This function is private and shouldn't be called from outside of the system itself.
 */
void _aos_sched_reschedule();


#endif /* end of include guard: AOS_SCHED_H_2RJAN3PE */

