#ifndef AOS_CONSTANTS_H_E6WTG0AS
#define AOS_CONSTANTS_H_E6WTG0AS


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_constants.h
 *
 * @brief Provides implementation of all the enumerations and constant system specific values.
 */

#include "aos_config.h"

/**
 * @brief hook types enumeration
 */
typedef enum _aos_hook_t {

	/// hook executed by the idle task
	AOS_HOOK_IDLE = 0,

	/// hook will be called every time task explicitly will end it's execution (TODO not implemented)
	AOS_HOOK_TASK_EXIT,

#if AOS_CHECK_FOR_STACK_CORRUPTION == 1
	/// hook executed when a stack corruption is detected, this specific one, exceptionally should block
	AOS_HOOK_STACK_CORRUPTION,
#endif

	/// out of memory hook, may occur during task creation process, should block
	AOS_HOOK_OOM,

	/// reset hook - executed when watchdog resets the system, due to a hang (TODO not implemented)
	AOS_HOOK_RESET,

#if AOS_IMPLEMENT_SYSTEM_HALTED
	/// executed when the system, due to internal error, has aborted executing tasks and tries to exit
	AOS_HOOK_SYSTEM_HALTED,
#endif

	/// used to determine the hook count
	AOS_HOOK_LAST
} aos_hook_t;



/**
 * @brief system status definition
 */
typedef enum _aos_sys_status_t {
	/// aos_init has not been called yet, system is in unknown condition
	AOS_SYS_UNINITIALIZED = 0,

	/// aos_init has been called, system is ready
	AOS_SYS_READY,

	/// aos_run has been called, tasks are running
	AOS_SYS_RUNNING,

	/// system is halted due to internal error
	AOS_SYS_HALTED,

	/// used to determine the state count
	AOS_SYS_STATUS_LAST
} aos_sys_status_t;


/**
 * @brief possible state in which task can reside
 */
typedef enum _aos_task_state_t {
	/// task has been explicitly suspended from execution - it will not be scheduled
	AOS_TASK_SUSPENDED = 0,

	/// task is ready to be executed, waiting for it's time slice
	AOS_TASK_READY,

	/// task is currently running
	AOS_TASK_RUNNING,

	/// task is pauzed, it either wait's for hardware or for semaphore
	AOS_TASK_PAUZED,

	/// task has ended up, it won't be scheduled any more. 
	AOS_TASK_STOPPED,

	/// used to count the number of stack states
	AOS_TASK_STATE_LAST
} aos_task_state_t;


/**
 * @brief predefined task priority values 
 */
typedef enum _aos_task_priority_t {
	/// highest possible priority - those tasks will be granted with maximum amount of quanta
	AOS_TASK_PRIORITY_HIGH = 0,

	/// normal task priority
	AOS_TASK_PRIORITY_NORMAL = 1,

	/// low task priority
	AOS_TASK_PRIORITY_LOW = 2,

	/// lowest possibly priority assigned to idle task. May be used by user tasks to denote very low task importance
	AOS_TASK_PRIORITY_IDLE,
} aos_task_priority_t;


#endif /* end of include guard: AOS_CONSTANTS_H_E6WTG0AS */

