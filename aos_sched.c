/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_sched.c
 *
 * @brief scheduler routines implementation
 */


#include "aos_sched.h"
#include "aos_common.h"
#include "aos_constants.h"
#include "aos_list.h"
#include "aos_timer.h"

#include <avr/io.h>
#include <avr/interrupt.h>

/* ================================================================================ */

/// global system context defined in aos_common
extern volatile struct aos_sys _g_sys;

/* ================================================================================ */

/**
 * @brief Translates priority to quanta
 *
 * @param a_prio priority
 *
 * @return number of quantas which process of given priority should receive
 */
static uint8_t _aos_sched_calculate_quanta(uint8_t a_prio);


/**
 * @brief generic implementation for sleeping/suspending tasks
 *
 * @param a_task task to be put to sleep/suspended
 * @param a_suspend 1 - suspend, 0 - sleep
 */
static void _aos_sched_task_hold(struct task_cb *a_task, uint8_t a_suspend);


/**
 * @brief generic implementation for awaking/resuming tasks
 *
 * @param a_task task to be awaken/resumed
 * @param a_resume 1 - resume, 0 - awake
 * @param a_ft 1 - called from tick interrupt, 0 - called from normal processing flow
 */
static void _aos_sched_task_bring_back(struct task_cb *a_task, uint8_t a_resume, uint8_t a_ft);

/* ================================================================================ */


void _aos_sched_tick_yield() {
	
#if AOS_CHECK_FOR_STACK_CORRUPTION == 1
	// check for stack corruption
	if (SP <= (uint16_t)((uint8_t *)_g_sys.current->wa + 4)) {
		// stack corruption detected and hold further execution
		_aos_common_hook_exec(AOS_HOOK_STACK_CORRUPTION);
		while (1);
	}
#endif

	//  process timers
	struct aos_timer *tm = _g_sys.timers;
	while (tm) {
		if (tm->value <= _g_sys.ticks) {
			if (tm->task->quanta)
				tm->task->quanta--;

			aos_list_remove((struct aos_list_entity **)&_g_sys.timers, 
					(struct aos_list_entity *)tm);

			aos_sched_task_resume(tm->task);				
			_aos_sched_reschedule();
			return;
		}

		tm = tm->nxt;
	}

	// do rescheduling if needed
	if (!_g_sys.current->quanta) {
		_aos_sched_reschedule();
	}
	else {
		// reduce task's time quanta
		_g_sys.current->quanta--;
	}
}


void _aos_sched_reschedule() {
	
	// register variables
	register uint8_t prio __asm__ ("r2");
	register uint8_t state __asm__ ("r3");
	register uint8_t cnt __asm__ ("r4");

	prio = aos_task_priority_get(_g_sys.current);
	state = aos_task_state_get(_g_sys.current);

	if (AOS_TASK_RUNNING == state) {
		aos_task_state_set(_g_sys.current, AOS_TASK_READY);	
		_g_sys.current->quanta = _aos_sched_calculate_quanta(prio);

		if (AOS_TASK_PRIORITY_IDLE != prio) {
			// move to expired queue
			aos_list_append((struct aos_list_entity **)&AOS_WAIT_LIST_GET(prio), 
					(struct aos_list_entity *)_g_sys.current);
		}
	}

	cnt = 2;
	while (cnt--) {

		// loop through task list
		for (prio = AOS_TASK_PRIORITY_HIGH; prio < AOS_TASK_PRIORITY_IDLE; prio++) {
			if (NULL == AOS_RUN_LIST_GET(prio))
				continue;

			_g_sys.current = 
				(struct task_cb *)aos_list_shift((struct aos_list_entity **)&AOS_RUN_LIST_GET(prio));
			break;
		}

		// switch queues as we ran out of tasks
		if (AOS_TASK_PRIORITY_IDLE == prio) {
			_g_sys.rl.active--;
		}
		else {
			// exit loop
			break;
		}
	}

	// schedule idle if there is nothing else available
	if (AOS_TASK_PRIORITY_IDLE == prio) {
		_g_sys.current = _g_sys.rl.idle;
	}

	aos_task_state_set(_g_sys.current, AOS_TASK_RUNNING);
}


__inline__
void aos_sched_task_sleep(struct task_cb *a_task) {
	_aos_sched_task_hold(a_task, 0);
}


__inline__
void aos_sched_task_awake(struct task_cb *a_task) {
	_aos_sched_task_bring_back(a_task, 0, 0);
}


__inline__
void aos_sched_task_suspend(struct task_cb *a_task) {
	_aos_sched_task_hold(a_task, 1);
}


__inline__
void aos_sched_task_resume(struct task_cb *a_task) {
	_aos_sched_task_bring_back(a_task, 1, 0);
}


__attribute__((naked))
void aos_sched_yield() {	
	AOS_LOCK();
	AOS_CTX_SAVE();
	AOS_CTX_SP_GET(_g_sys.current);

#if AOS_CHECK_FOR_STACK_CORRUPTION == 1
	// check for stack corruption
	if (SP <= (uint16_t)((uint8_t *)_g_sys.current->wa + 4)) {
		// stack corruption detected and hold further execution
		_aos_common_hook_exec(AOS_HOOK_STACK_CORRUPTION);
		while (1);
	}
#endif

	// switch context	
	_aos_sched_reschedule();

	AOS_CTX_SP_SET(_g_sys.current);
	AOS_CTX_RESTORE();
	AOS_UNLOCK();

	// ret will jump to new task
	AOS_CTX_POP_PC();
}

/* ================================================================================ */

static uint8_t _aos_sched_calculate_quanta(uint8_t a_prio) {
	uint8_t quanta_table[AOS_TASK_PRIORITY_IDLE] = {
		AOS_SCHED_TASK_QUANTA_PRIORITY_HIGH,
		AOS_SCHED_TASK_QUANTA_PRIORITY_NORMAL,
		AOS_SCHED_TASK_QUANTA_PRIORITY_LOW,
	};

	return (a_prio >= AOS_TASK_PRIORITY_IDLE) ?
	   	AOS_SCHED_TASK_QUANTA_PRIORITY_IDLE : quanta_table[a_prio];
}


static void _aos_sched_task_hold(struct task_cb *a_task, uint8_t a_suspend) {

	// priority register variable
	register uint8_t prio __asm__ ("r2");

	AOS_LOCK();
	aos_task_state_set(a_task, (a_suspend ? AOS_TASK_SUSPENDED : AOS_TASK_PAUZED));
	prio = aos_task_priority_get(a_task);

	if (_g_sys.current != a_task) {
		aos_list_remove((struct aos_list_entity **)&AOS_RUN_LIST_GET(prio),
			   	(struct aos_list_entity *)a_task);
	}

	if (a_suspend) {
		// place at the end when suspending
		aos_list_append((struct aos_list_entity **)&_g_sys.rl.wait,
				(struct aos_list_entity *)a_task);
	}
	else {
		// place at the beginning when pausing
		aos_list_prepend((struct aos_list_entity **)&_g_sys.rl.wait,
				(struct aos_list_entity *)a_task);
	}

	if ((_g_sys.current == a_task) && 
			(AOS_SYS_RUNNING == aos_common_sys_state_get())) {
		aos_sched_yield();
	}

	AOS_UNLOCK();
}


static void _aos_sched_task_bring_back(struct task_cb *a_task, uint8_t a_resume, uint8_t a_ft) {

	// priority register variable
	register uint8_t prio __asm__ ("r2");
	register uint8_t tmp __asm__ ("r3");

	// get priority
	prio = aos_task_priority_get(a_task);

	AOS_LOCK();

	// mark as ready 
	aos_task_state_set(a_task, AOS_TASK_READY);

	// remove from wait list
	aos_list_remove((struct aos_list_entity **)&_g_sys.rl.wait,
			(struct aos_list_entity *)a_task);

	if (a_resume) {
		// recalculate quanta
		a_task->quanta = _aos_sched_calculate_quanta(prio);
	}

	// prepends to the run-list
	aos_list_prepend((struct aos_list_entity **)&AOS_RUN_LIST_GET(prio),
			(struct aos_list_entity *)a_task);

	// don't try to reschedule if the system is not running
	tmp = aos_common_sys_state_get();
	if (AOS_SYS_RUNNING != tmp) {
		return;
	}

	// invoke yield only if called from outside of the tick irq
	if (!a_ft) {
		// reschedule if needed
		tmp = aos_task_priority_get(_g_sys.current);
		if (prio > tmp ) {
			aos_sched_yield();
		}
	}

	AOS_UNLOCK();
}

/* ================================================================================ */
