#ifndef __AOS_LIST_H__
#define __AOS_LIST_H__


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_list.h
 *
 * @brief generic list implementation for AOS.
 *
 * Routines exposed here can be used with custom user data structures.
 *
 * @example list_01.c
 * @example list_02.c
 *
 */


// pca include
#include "common.h"



/**
 * @brief This structure defines a minimal list interface.
 *
 * Other system structures can be casted to this one in order to use the generic list
 * implementation code. Thanks to that, the code doesn't need to rely on a specific data
 * type, it uses it's own.
 */
struct aos_list_entity {
	/// must be at the very beginning or else ... Demons will come out of the closed
	struct aos_list_entity *prv,*nxt;

	/// padding, this is not important from the list implementation point of view
	uint8_t data[1];
} __attribute__((packed));


/**
 * @brief macro used to declare the AOS list memory elements
 *
 * It's a macro which calculates the needed amount of memory area to hold the aos_list_entity
 * and the requested data structure. It allocates the requested number of elements
 *
 * @param __dt data type which will be used for the list
 * @param __name name of the variable
 * @param __elements number of elements requested
 *
 * @return 
 */
#define AOS_LIST_ALLOCATE(__dt, __name, __elements) \
	struct { \
		uint8_t m[sizeof(struct aos_list_entity) + \
			sizeof(__dt) - \
			sizeof(((struct aos_list_entity *)0)->data)];\
	} __name [__elements]


/**
 * @brief unlink entity from it's list
 *
 * The entity will be removed from the list and it's pointers will be set to NULL, meaning
 * it becomes a standalone member
 *
 * @param a_ent entity
 */
void aos_list_unlink(struct aos_list_entity *a_ent);


/**
 * @brief add a entity to the beginning of the list
 *
 * @param a_list list
 * @param a_ent entity
 */
void aos_list_prepend(struct aos_list_entity **a_list, struct aos_list_entity *a_ent);


/**
 * @brief add entity to the end of the list
 *
 * @param a_list list
 * @param a_ent entity
 */
void aos_list_append(struct aos_list_entity **a_list, struct aos_list_entity *a_ent);


/**
 * @brief remove entity from the beginning of the list
 *
 * @param a_list list
 *
 * @return entity that was removed
 */
struct aos_list_entity* aos_list_shift(struct aos_list_entity **a_list);


/**
 * @brief remove entity from the end of the list and return it
 *
 * @param a_list list
 *
 * @return entity from the end of list
 */
struct aos_list_entity* aos_list_pop(struct aos_list_entity **a_list);


/**
 * @brief remove list which resides in the middle of the list
 *
 * @param a_list list
 * @param a_ent entity
 */
void aos_list_remove(struct aos_list_entity **a_list, struct aos_list_entity *a_ent);


#endif /* __AOS_LIST_H__ */
