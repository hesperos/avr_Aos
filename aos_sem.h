#ifndef AOS_SEM_H_YD1R3WWK
#define AOS_SEM_H_YD1R3WWK


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_sem.h
 *
 * @brief semaphores interface
 *
 * Provides interface for semaphores in AOS.
 *
 * @example semaphores_01.c
 *
 * Brief example on using the semaphores
 *
 */


// pca include
#include "common.h"
#include "aos_task.h"


/**
 * @brief semaphore definition
 */
struct aos_sem {	
	/// task waiting on the semaphore
	struct task_cb *task;

	/// resource count
	uint8_t rcnt;
};


/**
 * @brief initialize a semaphore structure
 *
 * @param a_sem semaphore
 */
void aos_sem_init(struct aos_sem *a_sem);


/**
 * @brief signal operation, wakeup a thread if any is waiting on this semaphore
 *
 * @param a_sem semaphore
 */
void aos_sem_v(struct aos_sem *a_sem);


/**
 * @brief wait operation, decrement semaphore and sleep current thread if it reaches 0
 *
 * @param a_sem semaphore
 */
void aos_sem_p(struct aos_sem *a_sem);


/**
 * @brief check the semaphore value without changing it's state
 *
 * @param a_sem semaphore
 *
 * @return semaphore value
 */
uint8_t aos_sem_peek(struct aos_sem *a_sem);

#endif /* end of include guard: AOS_SEM_H_YD1R3WWK */

