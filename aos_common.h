#ifndef AOS_COMMON_H_ILSARTOX
#define AOS_COMMON_H_ILSARTOX


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_common.h
 *
 * @brief This file declares a common routines for AOS
 *
 * Contains common routines used by other parts of the AOS implementation.
 *
 */


#include "aos_sys_types.h"
#include "aos_config.h"


/**
 * @brief lock interrupts
 *
 * This call can be used by the tasks to protect a critical sections (protecting them from preemption)
 *
 * @return none
 */
#define AOS_LOCK() cli()


/**
 * @brief unlock interrupts
 *
 * This call can be used by the tasks to un-protect a critical sections (re-enabling preemption)
 *
 * @return none
 */
#define AOS_UNLOCK() sei()



/**
 * @brief Return pointer to the head of struct task_cb list being currently the active run list
 */
#define AOS_RUN_LIST_GET(__prio) \
	_g_sys.rl.prio_tl[__prio][_g_sys.rl.active & 0x01]


/**
 * @brief Return pointer to the head of struct task_cb list being currently the expired run list 
 */
#define AOS_WAIT_LIST_GET(__prio) \
	_g_sys.rl.prio_tl[__prio][(_g_sys.rl.active + 1) & 0x01]


/**
 * @brief initialize the system
 *
 * Initializes the internal system data structures to known conditions. Zeros _g_sys static variable,
 * creates an idle task and sets everything up, so the system is ready to accept new tasks being created
 * and to be started
 *
 * @param a_freq tick frequency
 */
void aos_init(uint32_t a_freq);


/**
 * @brief launch the scheduler and all tasks
 *
 * From now on, the system will start execution. This call is blocking and should never return.
 */
void aos_run();


/**
 * @brief install a specific system hook callback.
 *
 * The hook callbacks should not block and should be as short as possible. There is one exception to that rule,
 * which is the stack corruption detection hook callback - obviously you don't want to return from that hook, since
 * the system is probably in unknown condition.
 *
 * @param a_hook hook type
 * @param a_hook_cb hook callback - pointer to a function performing the hook action.
 */
void aos_common_hook_install(aos_hook_t a_hook, aos_hook_cb_t a_hook_cb);


/**
 * @brief execute a specified hook type
 *
 * This is a private API and should not be called from outside.
 *
 * @param a_hook hook type
 */
void _aos_common_hook_exec(aos_hook_t a_hook);


/**
 * @brief get the system state
 *
 * Returns current system state (have a look at aos_sys_status_t enumeration for possible values)
 *
 * @return system state enumeration
 */
aos_sys_status_t aos_common_sys_state_get();


/**
 * @brief return number of ticks since the system start
 *
 * @return number of ticks from system boot
 */
aos_systime_t aos_common_systime_get();


/**
 * @brief get the currently scheduled and running tasks control block
 *
 * @return control block
 */
struct task_cb* aos_common_current_get();


/**
 * @brief set the system state
 *
 * This is a private API and should not be called from outside.
 *
 * @param a_state appropriate system state value
 */
void _aos_common_sys_state_set(aos_sys_status_t a_state);


/**
 * @brief get the ISR interrupted flag
 *
 * This is a private API and should not be called from outside. Check if tick interrupt interrupted an ISR.
 *
 * @return true if tick interrupt interrupted an ISR
 */
uint8_t _aos_common_isr_interrupted_get();

/**
 * @brief set the ISR interrupted flag
 *
 * This is a private API and should not be called from outside.
 *
 * @param a_isr_int_flag set if tick interrupt interrupted an ISR
 */
void _aos_common_isr_interrupted_set(uint8_t a_isr_int_flag);

#endif /* end of include guard: AOS_COMMON_H_ILSARTOX */

