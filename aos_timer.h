#ifndef AOS_TIMER_H_LOMGE6ZV
#define AOS_TIMER_H_LOMGE6ZV


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_timer.h
 *
 * @brief Timer implementation for AOS
 *
 * Contains timer structure declaration and interface for using timers in AOS.
 * Task can pause itself for a specified amount of ticks by using timers. That way
 * it gives the processing time to other task.
 *
 * @example timer_01.c
 *
 * Using timer_wait as a non-blocking delay, in order not to waste CPU time and let
 * other tasks use it.
 */


#include "aos_sys_types.h"
#include "aos_task.h"


/**
 * @brief timer declaration
 */
struct aos_timer {
	/// list pointers must be on top in order to benefit from aos_list implementation
	struct aos_timer *prv, *nxt;

	/// timer value
	aos_systime_t value;

	/// task pointer
	struct task_cb *task;
};


/**
 * @brief pauses the task for a specified amount of ticks.
 *
 * After calling this function the task will not be scheduled until
 * the requested amount of ticks pass by
 *
 * @param a_timer pointer to the timer structure
 * @param value defines how many ticks the process wants to sleep
 */
void aos_timer_wait(struct aos_timer *a_timer, aos_systime_t value);


#endif /* end of include guard: AOS_TIMER_H_LOMGE6ZV */

