#ifndef AOS_CONFIG_H_ZOBEPOCT
#define AOS_CONFIG_H_ZOBEPOCT


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_config.h
 *
 * @brief This file stores some basic configuration variables for AOS
 *
 * This file stores an AOS global options available to the user. One may customize this values
 * without modifying the actual implementation. 
 *
 */


// ================================================================================

/**
 * @brief which timer to use as a tick generator
 */
#define AOS_SCHED_TIMER 2

/**
 * @brief stack size for the IDLE task
 */
#define AOS_COMMON_IDLE_TASK_STACK_SIZE 32

/**
 * @brief should aos check every tick if stack pointer of current task exceeded valid value range
 */
#define AOS_CHECK_FOR_STACK_CORRUPTION 1

/**
 * @brief implement support for system halted hook
 */
#define AOS_IMPLEMENT_SYSTEM_HALTED 0

/**
 * @brief number of quanta assigned for HIGH priority tasks
 */
#define AOS_SCHED_TASK_QUANTA_PRIORITY_HIGH 	196

/**
 * @brief number of quanta assigned for NORMAL priority tasks
 */
#define AOS_SCHED_TASK_QUANTA_PRIORITY_NORMAL 	32

/**
 * @brief number of quanta assigned for LOW priority tasks
 */
#define AOS_SCHED_TASK_QUANTA_PRIORITY_LOW 		8

/**
 * @brief number of quanta assigned for IDLE priority tasks
 */
#define AOS_SCHED_TASK_QUANTA_PRIORITY_IDLE 	2

// ================================================================================

#endif /* end of include guard: AOS_CONFIG_H_ZOBEPOCT */

