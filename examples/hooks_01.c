#include "aos.h"

#include <avr/io.h>
#include <util/delay.h>

// tick every 10 ms
#define AOS_TICK_FREQUENCY 100


/**
 * @brief some dummy system data
 */
struct system_data {
	uint32_t cntr1;
	uint32_t cntr2;
};


/**
 * @brief this hook will be called when we discover that stack corruption occured
 *  it should block, since the system become faulty.
 */
void hook_stack_corruption(void) {

	// indicate stack corruption and block
	PORTB |= 0x01;
	while (1);
}


/**
 * @brief this hook will be called when we discover that there is no memory left,
 *  during task creation.
 */
void hook_oom(void) {

	// indicate out of memory and block
	PORTB |= 0x02;
	while (1);
}


/**
 * @brief this hook will be called whenever idle task is scheduled for execution
 */
void hook_idle(void) {
	// toggle LED if executing idle
	PORTB ^= 0x80;
}


void task1(void *a_data UNUSED) {
	struct system_data *sdata = (struct system_data *)a_data;
	while (1) {
		sdata->cntr1++;
	}
}


void task2(void *a_data UNUSED) {
	struct system_data *sdata = (struct system_data *)a_data;
	while (1) {
		sdata->cntr2++;
	}
}


int main(void) {

	// PORTB will be the "status port", indicating any system problem
	DDRB = 0xff;
	PORTB = 0x00;

	struct system_data data;

	// initialize the system
	aos_init(AOS_TICK_FREQUENCY);

	// install oom hook
	aos_common_hook_install(AOS_HOOK_OOM, hook_oom);

	// install stack corruption hook
	aos_common_hook_install(AOS_HOOK_STACK_CORRUPTION, hook_stack_corruption);

	// install idle hook
	aos_common_hook_install(AOS_HOOK_IDLE, hook_idle);

	// create tasks
	aos_task_create(task1, (void *)&data, AOS_TASK_PRIORITY_NORMAL, 48);
	aos_task_create(task2, (void *)&data, AOS_TASK_PRIORITY_NORMAL, 48);

	// start the scheduler
	aos_run();
	return 0;
}
