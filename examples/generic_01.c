#include "aos.h"

#include <avr/io.h>
#include <util/delay.h>

// tick every 10 ms
#define AOS_TICK_FREQUENCY 100


void task1(void *a_data UNUSED) {
	// initialization
	DDRB = 0xff;
	PORTB = 0x00;

	while (1) {
		PORTB ^= 0xff;
		// busy loop - will consume a lot of processing time
		_delay_ms(100);
	}
}


void task2(void *a_data UNUSED) {
	// initialization
	DDRC = 0xff;
	PORTC = 0x00;

	while (1) {
		PORTC ^= 0xff;

		// busy loop - will consume a lot of processing time
		_delay_ms(200);
	}
}


void task3(void *a_data UNUSED) {
	// initialization
	DDRD = 0xff;
	PORTD = 0x00;

	while (1) {
		PORTD ^= 0xff;
		
		// busy loop - will consume a lot of processing time
		_delay_ms(300);
	}
}


int main(void) {

	// initialize the system
	aos_init(AOS_TICK_FREQUENCY);

	// create tasks
	aos_task_create(task1, NULL, AOS_TASK_PRIORITY_NORMAL, 32);
	aos_task_create(task2, NULL, AOS_TASK_PRIORITY_NORMAL, 32);
	aos_task_create(task3, NULL, AOS_TASK_PRIORITY_NORMAL, 32);

	// start the scheduler
	aos_run();
	return 0;
}
