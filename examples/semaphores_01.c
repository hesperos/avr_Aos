#include "aos.h"

#include <avr/io.h>
#include <util/delay.h>

// tick every 10 ms
#define AOS_TICK_FREQUENCY 100

// semaphore definition
static struct aos_sem semaphore;

void task1(void *a_data UNUSED) {
	// initialization
	struct aos_timer tm;
	DDRB = 0xff;
	PORTB = 0x00;

	// initialize semaphore
	aos_sem_init(&semaphore);

	while (1) {
		PORTB ^= 0xff;

		// wait until semaphore available
		aos_sem_p(&semaphore);
	}
}


void task2(void *a_data UNUSED) {
	// initialization
	struct aos_timer tm;
	DDRC = 0xff;
	PORTC = 0x00;

	while (1) {
		PORTC ^= 0xff;

		// pauze this task for 20 ticks, so other can benefit
		aos_timer_wait(&tm, 20);

		// signal semaphore, thus awakening task1
		aos_sem_v(&semaphore);
	}
}


void task3(void *a_data UNUSED) {
	// initialization
	struct aos_timer tm;
	DDRD = 0xff;
	PORTD = 0x00;

	while (1) {
		PORTD ^= 0xff;
		
		// pauze this task for 30 ticks, so other can benefit
		aos_timer_wait(&tm, 30);
	}
}


int main(void) {

	// initialize the system
	aos_init(AOS_TICK_FREQUENCY);

	// create tasks
	aos_task_create(task1, NULL, AOS_TASK_PRIORITY_NORMAL, 48);
	aos_task_create(task2, NULL, AOS_TASK_PRIORITY_NORMAL, 48);
	aos_task_create(task3, NULL, AOS_TASK_PRIORITY_NORMAL, 48);

	// start the scheduler
	aos_run();
	return 0;
}
