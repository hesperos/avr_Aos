#include "aos.h"

#include <avr/io.h>
#include <util/delay.h>

// tick every 10 ms
#define AOS_TICK_FREQUENCY 100

struct custom_data_list {
	uint8_t num;
};


AOS_LIST_ALLOCATE(struct custom_data_list, elements, 32);

struct aos_list_entity *list1 = NULL;


void task1(void *a_data UNUSED) {
	struct aos_list_entity *current = list1;
	DDRB = 0xff;

	while (1) {

		// traverse the list
		if (NULL == current)
			continue;

		PORTB = ((struct custom_data_list *)current->data)->num;
		current = current->nxt;

		// go to begining, if end has been reached
		if (NULL == current)
			current = list1;

		_delay_ms(500);
	}
}


void task2(void *a_data UNUSED) {
	struct aos_list_entity *current = list1;
	DDRD = 0xff;

	while (1) {

		// traverse the list backwards
		if (NULL == current)
			continue;

		PORTD = ((struct custom_data_list *)current->data)->num;
		current = current->prv;

		// current will never be null, since
		// list_head->prv == last element

		_delay_ms(500);
	}
}


int main(void) {

	// create list
	uint8_t x = 0;
	while (x<32) {
		elements[x].num = x;
		aos_list_prepend((struct aos_list_entity **)&list1,
				(struct aos_list_entity *)&elements[x]);
		x++;
	}

	// initialize the system
	aos_init(AOS_TICK_FREQUENCY);

	// create tasks
	aos_task_create(task1, NULL, AOS_TASK_PRIORITY_NORMAL, 64);
	aos_task_create(task2, NULL, AOS_TASK_PRIORITY_NORMAL, 64);

	// start the scheduler
	aos_run();
	return 0;
}
