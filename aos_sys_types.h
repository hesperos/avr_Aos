#ifndef AOS_SYS_TYPES_H_JIZQJ45Z
#define AOS_SYS_TYPES_H_JIZQJ45Z


/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_sys_types.h
 *
 * @brief generic system types.
 *
 * Contains declaration of system time data type, global system context, run list and many more
 */


#include "common.h"
#include "aos_ctx.h"
#include "aos_constants.h"

#include <stdlib.h>

/**
 * @brief system time type definition (in ticks)
 */
typedef vuint32_t aos_systime_t;


/**
 * @brief aos system hook type definition
 *
 * @param none
 *
 * @return none
 */
typedef void (*aos_hook_cb_t)(void);


/**
 * @brief defines the system run-list
 */
struct aos_run_list {

	/**
	 * @brief defines a separate list for each priority, grouping the tasks of same priorities
	 *
	 * There are two run-lists. The active and expired one. The active list is selected by the 
	 * active field. The scheduler works in a way that it schedules the task in priority order
	 * from the active list and moves them to the expired list once they're time quanta goes down
	 * to zero. Once there are no tasks available in the active list the active variable is switched to
	 * select the expired list as the active one (active = (active + 1) & 0x01).
	 *
	 */
	struct task_cb *prio_tl[AOS_TASK_PRIORITY_IDLE][2];

	/**
	 * @brief idle task
	 */
	struct task_cb *idle;

	/**
	 * @brief contains tasks which are either paused or suspended
	 */
	struct task_cb *wait;

	/**
	 * @brief defines the active & expired lists
	 */
	uint8_t active;
};


/**
 * @brief system state descriptor
 */
struct aos_sys {
	
	/**
	 * @brief holds global stack pointer and system context
	 *  maybe useful for doing a task fork implementation
	 */
	volatile struct aos_ctx ctx;
	
	/// hook dispatching table
	aos_hook_cb_t hooks[AOS_HOOK_LAST];

	/// system time
	aos_systime_t ticks;

	/// task currently scheduled
	struct task_cb *current;

	/// system run-list
	struct aos_run_list rl;

	/// list of timers if there are any
	struct aos_timer *timers;

	/**
	 * @verbatim
     * 
	 * 	         | B |  A
	 * - - - - - | - | - -
	 * 7 6 5 4 3 | 2 | 1 0
	 * -------------------
	 *
	 * A - System Status
	 * B - ISR interrupted flag
	 * 
	 * @endverbatim
	 */
	/// system status register
	uint8_t status;
};


#endif /* end of include guard: AOS_SYS_TYPES_H_JIZQJ45Z */
