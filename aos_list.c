/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_list.c
 *
 * @brief
 */


#include "aos_list.h"

void aos_list_unlink(struct aos_list_entity *a_ent) {
	if (NULL != a_ent->prv) {
		a_ent->prv->nxt = a_ent->nxt;
		a_ent->prv = NULL;
	}

	if (NULL != a_ent->nxt) {
		a_ent->nxt->prv = a_ent->prv;
		a_ent->nxt = NULL;
	}
}


void aos_list_remove(struct aos_list_entity **a_list, struct aos_list_entity *a_ent) {

	if (NULL == *a_list) {
		return;
	}

	if ((*a_list) == a_ent) {
		aos_list_shift(a_list);
		return;
	}

	if ((*a_list)->prv == a_ent) {
		aos_list_pop(a_list);
		return;
	}

	aos_list_unlink(a_ent);
}


struct aos_list_entity* aos_list_shift(struct aos_list_entity **a_list) {

	struct aos_list_entity *ptr = *a_list;	

	if (NULL == (*a_list)) {
		return NULL;
	}

	*a_list = ptr->nxt;
	if (NULL != (*a_list))
		(*a_list)->prv = ptr->prv;

	ptr->prv = NULL;
	ptr->nxt = NULL;

	return ptr;
}


struct aos_list_entity* aos_list_pop(struct aos_list_entity **a_list) {
	
	struct aos_list_entity *ptr = NULL;

	if (NULL == (*a_list)) {
		return NULL;
	}

	ptr = (*a_list)->prv;

	if (NULL == (*a_list)->nxt) {
		*a_list = NULL;		
	}
	else {
		(*a_list)->prv = ptr->prv;
	}
	
	aos_list_unlink(ptr);
	return ptr;
}


void aos_list_prepend(struct aos_list_entity **a_list, struct aos_list_entity *a_ent) {
	a_ent->nxt = *a_list;

	if (NULL != *a_list) {
		a_ent->prv = (*a_list)->prv;
		(*a_list)->prv = a_ent;
	}
	else {
		a_ent->prv = a_ent;
	}

	*a_list = a_ent;
}


void aos_list_append(struct aos_list_entity **a_list, struct aos_list_entity *a_ent) {
	if (NULL != (*a_list)) {
		(*a_list)->prv->nxt = a_ent;
		a_ent->prv = (*a_list)->prv;
		(*a_list)->prv = a_ent;
	}
	else {
		a_ent->prv = a_ent;
		a_ent->nxt = NULL;
		*a_list = a_ent;
	}
}
