/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

/**
 * @file aos_common.c
 *
 * @brief
 */


#include "aos_common.h"
#include "aos_task.h"
#include "aos_sched.h"
#include "timer_common.h" // libpca deliverable

#include <avr/interrupt.h>
#include <string.h>
#include <avr/sleep.h>

/* ================================================================================ */

/**
 * @brief AOS system status holder, global variable
 */
volatile struct aos_sys _g_sys;

/* ================================================================================ */

/**
 * @brief idle task definition
 *
 * @param a_data private data for the task
 */
static void _aos_idle_task(void *a_data UNUSED) {

	// busy loop
	while (1) {
		_aos_common_hook_exec(AOS_HOOK_IDLE);

#if AOS_SCHED_TIMER == 2
		// if TIMER2 is used we can sleep the core
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_enable();
		sleep_cpu();
		sleep_disable();
#endif
	}
}

/* ================================================================================ */

/**
 * @brief tick interrupt definition
 */
#if AOS_SCHED_TIMER == 0
ISR(TIMER0_COMPA_vect, ISR_NAKED)
#elif AOS_SCHED_TIMER == 1
ISR(TIMER1_COMPA_vect, ISR_NAKED)
#elif AOS_SCHED_TIMER == 2
ISR(TIMER2_COMPA_vect, ISR_NAKED)
#else
#error AOS: UNKNOWN TIMER REQUESTED, ALLOWED VALUES ARE (0..2)
#endif
{
	// save machine context
	AOS_CTX_SAVE();

	// ensure not to context switch when tick interrupt happened
	// in between the actual interrupt
	if (!_aos_common_isr_interrupted_get()) {

		// interrupts are disabled by default anyway, but just as a precaution
		AOS_LOCK();	

		// save SP to current task ctx
		AOS_CTX_SP_GET(_g_sys.current);

		// increment number of sys-ticks and ticks that task has consumed
		_g_sys.ticks = _g_sys.ticks + 1;
		_g_sys.current->systicks = 
			_g_sys.current->systicks + 1;

		// find next task
		_aos_sched_tick_yield();

		// restore machine context
		AOS_CTX_SP_SET(_g_sys.current);
	}

	// restore the context
	AOS_CTX_RESTORE();	

	// leave interrupt service routine	
	reti();
}

/* ================================================================================ */

void aos_init(uint32_t a_freq) {
	// timer pre-scaler values
	uint32_t pocr = 0x00;

	// lock interrupts
	AOS_LOCK();

	// initialize system data struct
	memset((void *)&_g_sys, 0x00, sizeof(struct aos_sys));

	// setup initial system status
	_aos_common_sys_state_set(AOS_SYS_UNINITIALIZED);

	// store system stack pointer value
	_g_sys.ctx.sp = (struct aos_machine_ctx *)SP;

	// timer setup
	_timer_init_ctc(AOS_SCHED_TIMER);
	if (E_TIMER1 == AOS_SCHED_TIMER) {
		pocr = _timer_freq_prescale(E_TIMER1, a_freq, 65535);
	}
	else {
		pocr = _timer_freq_prescale(AOS_SCHED_TIMER, a_freq, 255);
	}
	
	_timer_setup_ctc(AOS_SCHED_TIMER, pocr);

	// select active task list
	_g_sys.rl.active = 0;

	// create idle task
	_g_sys.current = aos_task_create(_aos_idle_task, 
			NULL, 
			AOS_TASK_PRIORITY_IDLE, 
			AOS_COMMON_IDLE_TASK_STACK_SIZE);

	// setup as idle task
	_g_sys.rl.idle = _g_sys.current;
	_g_sys.rl.idle->quanta = AOS_SCHED_TASK_QUANTA_PRIORITY_IDLE;

	// mark as running
	aos_task_state_set(_g_sys.current, AOS_TASK_RUNNING);

	// setup system status
	_aos_common_sys_state_set(AOS_SYS_READY);

	// unlock interrupts
	AOS_UNLOCK();
}


void aos_common_hook_install(aos_hook_t a_hook, aos_hook_cb_t a_hook_cb) {
	_g_sys.hooks[a_hook] = a_hook_cb;
}


__inline__
void _aos_common_hook_exec(aos_hook_t a_hook) {
	// execute hook if it exists
	if (NULL != _g_sys.hooks[a_hook]) _g_sys.hooks[a_hook]();
}


void aos_run() {
	// enable the tick interrupt	
	_timer_en_compa_int(AOS_SCHED_TIMER);

	// at this point mark the system as running
	_aos_common_sys_state_set(AOS_SYS_RUNNING);

	// set sp to current task's context and restore it
	AOS_CTX_SP_SET(_g_sys.current);
	AOS_CTX_RESTORE();

	// unlock global interrupts, since context have those disabled
	AOS_UNLOCK();

	// jump to first task
	AOS_CTX_POP_PC();

	// this point should never be reached
	while(1) {
#if AOS_IMPLEMENT_SYSTEM_HALTED
		_aos_common_hook_exec(AOS_HOOK_SYSTEM_HALTED);
#endif
	}
}


__inline__
aos_systime_t aos_common_systime_get() {
	return (_g_sys.ticks);
}


__inline__
aos_sys_status_t aos_common_sys_state_get() {
	return (_g_sys.status & 0x03);
}


__inline__
void _aos_common_sys_state_set(aos_sys_status_t a_state) {
	_g_sys.status &= 0xfc;
	_g_sys.status |= (a_state & 0x03);
}


__inline__
uint8_t _aos_common_isr_interrupted_get() {
	return ((_g_sys.status & 0x04) ? 0x01: 0x00);
}


__inline__
struct task_cb* aos_common_current_get() {
	return _g_sys.current;
}


__inline__
void _aos_common_isr_interrupted_set(uint8_t a_isr_int_flag) {
	if (a_isr_int_flag)
		_g_sys.status |= 0x04;
	else
		_g_sys.status &= 0xfb;
}


void aos_init_watchdog() {
	AOS_LOCK();
	// TODO implement me 
	AOS_UNLOCK();
}

void aos_clear_watchdog() {
	
	// TODO implement me 
}

/* ================================================================================ */
