TARGET=libaos.a
SOURCES= \
		 aos_task.c \
		 aos_sched.c \
		 aos_list.c \
		 aos_timer.c \
		 aos_sem.c \
		 aos_common.c

DEPS=
COBJ=$(SOURCES:.c=.o)
PCA_PREFIX=../pca

CC=avr-gcc
AR=avr-ar
STRIP=avr-strip
MCU=atmega328p
CFLAGS=-I. -I$(PCA_PREFIX)/include/ -Wall -Os -DF_CPU=16000000UL -std=c99 -ffunction-sections

all: $(TARGET)

%.o: %.c $(DEPS)
	@echo -e "\tCC" $<
	@$(CC) -mmcu=$(MCU) -c -o $@ $< $(CFLAGS)

$(TARGET): $(COBJ) 
	@echo -e "\tARCHIVING CC" $(COBJ)				
	$(STRIP) -g $(COBJ)
	$(AR) rcsv $(TARGET) $(COBJ)

clean:
	@echo ========== cleanup ========== 
	rm -rf *.o $(TARGET)	
