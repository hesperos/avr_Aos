/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_sem.c
 *
 * @brief
 */


#include "aos_sem.h"
#include "aos_sched.h"
#include "aos_sys_types.h"

#include <string.h>
#include <avr/interrupt.h>


/* ================================================================================ */

extern volatile struct aos_sys _g_sys;

/* ================================================================================ */


void aos_sem_init(struct aos_sem *a_sem) {
	// simple as that
	memset(a_sem, 0x00, sizeof(struct aos_sem));
}


void aos_sem_v(struct aos_sem *a_sem) {
	AOS_LOCK();

	if (NULL == a_sem->task) {
		a_sem->rcnt++;
	}
	else {
		aos_sched_task_awake(a_sem->task);
	}
	AOS_UNLOCK();
}


void aos_sem_p(struct aos_sem *a_sem) {
	AOS_LOCK();

	if (a_sem->rcnt) {
		a_sem->rcnt--;
		AOS_UNLOCK();
		return;
	}
	
	a_sem->task = _g_sys.current;
	aos_sched_task_sleep(_g_sys.current);
	AOS_UNLOCK();
}


uint8_t aos_sem_peek(struct aos_sem *a_sem) {
	return a_sem->rcnt;	
}

/* ================================================================================ */
