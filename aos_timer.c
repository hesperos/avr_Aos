/* Copyright (C) 
 * 2013 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


/**
 * @file aos_timer.c
 *
 * @brief contains implementation of timers for AOS
 */


#include "aos_timer.h"
#include "aos_common.h"
#include "aos_list.h"
#include "aos_sched.h"

#include <string.h>
#include <avr/interrupt.h>

/* ================================================================================ */

/**
 * @brief global system context, declared in aos_common
 */
extern volatile struct aos_sys _g_sys;

/* ================================================================================ */

void aos_timer_wait(struct aos_timer *a_timer, aos_systime_t value) {

	memset(a_timer, 0x00, sizeof(struct aos_timer));
	a_timer->task = _g_sys.current;

	AOS_LOCK();
	a_timer->value = _g_sys.ticks + value;

	aos_list_append((struct aos_list_entity **)&_g_sys.timers, 
			(struct aos_list_entity *)a_timer);

	aos_sched_task_sleep(_g_sys.current);
	AOS_UNLOCK();
}

/* ================================================================================ */
